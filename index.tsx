import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from "mobx-react"
import ExchangeStore from './src/stores/exchangeStore'
import { MainPage } from "./src/mainPage";

ReactDOM.render(
   <Provider exchangeStore={ExchangeStore} >
     <MainPage />
   </Provider>
   ,
   document.getElementById("root")
);



