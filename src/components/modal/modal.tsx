import * as React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { inject } from 'mobx-react';
import './modal.css'
import { IExchange } from 'interfaces';

interface ModalProps {
  exchange: IExchange;
}

@inject('exchangeStore')
export default class Modal extends React.Component<any, any> {
  private date: HTMLInputElement
  private rate: HTMLInputElement

  submit = () => {
    const { save, exchange, exchangeStore } = this.props
    exchangeStore.updateExchange({
      ...exchange,
      rate: Number(this.rate.value),
      date: this.date.value,
      editing: false
    })
  }
  componentDidMount() {
    const { exchange } = this.props
    this.date.value = exchange.date
    this.rate.value = exchange.rate
  }
  render() {
    const { exchange, exchangeStore } = this.props
    return (
      <div className={`modal fade ${exchange ? 'show opened' : ''}`}>
        <div className='modal-dialog modal-sm'>
          <div className='modal-content'>
            <div className='modal-body'>
              <form>
                <div className='form-group'>
                  <label className='form-control-label'>Date:</label>
                  <input ref={(ref) => this.date = ref} type='datetime' className='form-control' />
                </div>
                <div className='form-group'>
                  <label className='form-control-label'>Rate:</label>
                  <input ref={(ref) => this.rate = ref} type='number' className='form-control' />
                </div>
              </form>
            </div>
            <div className='modal-footer'>
              <button onClick={exchangeStore.resetEditing} type='button' className='btn btn-secondary'>Close</button>
              <button onClick={this.submit} type='button' className='btn btn-primary'>Save changes</button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}