import * as React from 'react';
import { IExchange } from 'interfaces';
import 'bootstrap/dist/css/bootstrap.min.css';
import './table.css'

interface TableProps {
  data?: Array<IExchange>;
  edit?: Function;
};

const Table: React.SFC<TableProps> = (props: any) => {
  const { data, edit } = props
  return (
    <table className='table'>
      <thead>
        <tr>
          <th>#</th>
          <th>Currency pair</th>
          <th>Date</th>
          <th>Rate</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {data.map((item: any, key: number) => {
          return <tr key={key}>
            <td>{item.id}</td>
            <td>{item.currencyPair}</td>
            <td>{item.date}</td>
            <td>{item.rate}</td>
            <td>
              <a onClick={() => edit(item.id)}>
                <i className='far fa-edit'></i>
              </a>
            </td>
          </tr>
        })}
      </tbody>
    </table>
  );
};

export default Table;