export interface IExchange {
  id: number;
  currencyPair: string;
  date: string;
  rate: number;
  editing: boolean;
};