import * as React from 'react';
import { Table, Modal } from './components';
import { observer, inject } from 'mobx-react';
import './style.css';
import 'bootstrap/dist/css/bootstrap.min.css';

@inject('exchangeStore')
@observer
export class MainPage extends React.Component<any, any> {
    componentDidMount() {
        this.props.exchangeStore.getTable()
    }
    render() {
        const { exchangeStore } = this.props
        return (
            <div>
                <div className='mainPage'>
                    <div className='col-sm-6 offset-sm-3'>
                        <div className='box'>
                            <div className='box-header'>
                                <h3>Currency table</h3>
                            </div>
                            <Table
                                data={exchangeStore.exchanges}
                                edit={(id: number) => exchangeStore.updateEditingStatus(id)}
                            />
                        </div>
                    </div>

                </div>
                {exchangeStore.exchanges.find((item: any) => item.editing) &&
                    <Modal
                        exchange={exchangeStore.exchanges.find((item: any) => item.editing)}
                    />}
            </div>
        );
    }
}






