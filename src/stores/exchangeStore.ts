import { observable, action } from 'mobx'
import { IExchange } from 'interfaces'
import axios from 'axios'
import { xml2json } from 'helpers'
import * as moment from 'moment'

class ExchangeStore {
  @observable
  exchanges: IExchange[] = []

  @observable
  loading: boolean = false

  @action
  updateExchange(exchange: IExchange) {
    new Promise((resolve) => {
      //PUT
      resolve()
    }).then(response => {
      //if response is ok
      this.exchanges = this.exchanges.map(item => {
        if (item.id === exchange.id) {
          item = exchange
        }
        return item
      })
    })
  }

  @action
  updateEditingStatus(id: number) {
    this.exchanges = this.exchanges.map((item: any) => {
      item.editing = item.id === id
      return item;
    })
  }

  @action
  resetEditing = () => {
    this.exchanges = this.exchanges.map((item: any) => {
      item.editing = false
      return item;
    })
  }

  @action
  getTable() {
    this.loading = true
    axios.get('https://forex.1forge.com/1.0.3/quotes?pairs=EURUSD,GBPJPY,AUDUSD&api_key=0AdRQv0zied7aJQMyKEtiZ1I5F15cnZp&format=XML')
    .then(response => {
      const parser = new DOMParser()
      const xml = parser.parseFromString(response.data, 'text/xml')
      const json = xml2json(xml)
      console.log('json', json)
      this.exchanges = json.xml.item.map((item: any, key: number) => {
        return {
          id: key,
          currencyPair: item.symbol['#text'],
          date: moment.unix(item.timestamp['#text']).format("MM/DD/YYYY"),
          rate: item.price['#text'],
          editing: false
        }
      })
      this.loading = false
    })
  }
}

const observableExchangeStore = new ExchangeStore();
export default observableExchangeStore