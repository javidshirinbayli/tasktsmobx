CREATE TABLE Purchases (
  ID int NOT NULL PRIMARY KEY,
  UserId int NOT NULL,
  CurrencyTypeId: varchar(10) NOT NULL,
  Amount: decimal NOT NULL,
  PurchaseDate: datetime NOT NULL,
  Description: varchar(200),
  FOREIGN KEY (UserId) REFERENCES Users(ID), -- assuming
  FOREIGN KEY (CurrencyTypeId) REFERENCES Currencies(ID) -- assuming
)